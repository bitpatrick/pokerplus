package it.prismaprogetti.giochi.poker.model;

import static it.prismaprogetti.giochi.poker.model.Seme.*;

import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Queue;

import it.prismaprogetti.giochi.poker.utility.RandomUtility;


public class Mazzo {

	private final String marca;
	private final Queue<Carta> carte; // FIFO

	private Mazzo(String marca, Queue<Carta> carte) {
		super();
		this.marca = marca;
		this.carte = carte;
	}

	public String getMarca() {
		return marca;
	}

	public Queue<Carta> getCarte() {
		return carte;
	}

	public static Mazzo nuovoMazzoCartePoker(String nomeMarcaDelMazzo) {

		Queue<Carta> carte = new ArrayDeque<Carta>(52);

		/*
		 * creo 52 carte 13 carte per ogni seme
		 */
		boolean assoDiCuoriCreato = false;
		for (int i = 1; i <= 13; i++) {
			
			Carta carta = null;
			if ( !assoDiCuoriCreato ) {
				
				carta = Carta.crea(CUORI, Valore.creaValoriCartePoker(14));
				assoDiCuoriCreato = true;
			} else {
				
				carta = Carta.crea(CUORI, Valore.creaValoriCartePoker(i));
			}
			carte.add(carta);
		}

		boolean assoDiQuadriCreato = false;
		for (int i = 1; i <= 13; i++) {
			
			Carta carta = null;
			if ( !assoDiQuadriCreato ) {
				
				carta = Carta.crea(QUADRI, Valore.creaValoriCartePoker(14));
				assoDiQuadriCreato = true;
			} else {
				
				carta = Carta.crea(QUADRI, Valore.creaValoriCartePoker(i));
			}
			carte.add(carta);
		}

		boolean assoDiFioriCreato = false;
		for (int i = 1; i <= 13; i++) {
			
			Carta carta = null;
			if ( !assoDiFioriCreato ) {
				
				carta = Carta.crea(FIORI, Valore.creaValoriCartePoker(14));
				assoDiFioriCreato = true;
			} else {
				
				carta = Carta.crea(FIORI, Valore.creaValoriCartePoker(i));
			}
			carte.add(carta);
		}

		boolean assoDiPiccheCreato = false;
		for (int i = 1; i <= 13; i++) {
			
			Carta carta = null;
			if ( !assoDiPiccheCreato ) {
				
				carta = Carta.crea(PICCHE, Valore.creaValoriCartePoker(14));
				assoDiPiccheCreato = true;
			} else {
				
				carta = Carta.crea(PICCHE, Valore.creaValoriCartePoker(i));
			}
			carte.add(carta);
		}

		return new Mazzo(nomeMarcaDelMazzo, carte);
	}

	public void mischiaCarte() {

		Carta[] carte = new Carta[ this.carte.size() ];

		while (this.carte.size() != 0) {

			int indiceCasuale = RandomUtility.randomNumber(0, carte.length - 1);

			if (carte[indiceCasuale] != null) {
				continue;
			}

			carte[indiceCasuale] = this.carte.poll();

		}

		this.carte.addAll(Arrays.asList(carte));

	}

	public Carta[] daiCinqueCarte() {

		Carta[] carte = new Carta[5];

		for (int i = 0; i < 5; i++) {

			carte[i] = this.carte.poll();

		}

		return carte;

	}
	
	public Carta prendiCartaOrNull(String simbolo, Seme seme) {
		
		for ( Carta carta : this.carte) {
			
			if ( carta.getValore().getSimbolo() == simbolo && carta.getSeme() == seme) {
				
				return carta;
			}
		}
		
		return null;
	
	}

}
