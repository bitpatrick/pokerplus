package it.prismaprogetti.giochi.poker.model.punteggio;

import it.prismaprogetti.giochi.poker.model.Carta;

public class CartaPiuAlta implements Punto {

	private final Carta cartaPiuAlta;

	public CartaPiuAlta(Carta cartaPiuAlta) {
		super();
		this.cartaPiuAlta = cartaPiuAlta;
	}

	public Carta getCartaPiuAlta() {
		return cartaPiuAlta;
	}

	public static Carta valuta(Carta[] carte) {

		Carta cartaSingola = null;

		if (carte != null) {

			for (Carta carta : carte) {

				if (cartaSingola == null) {

					cartaSingola = carta;

				} else {

					if (carta.compareTo(cartaSingola) < 0) {

						cartaSingola = carta;

					}

				}

			}

		}

		return cartaSingola;

	}

	@Override
	public String info() {
		
		if (this.cartaPiuAlta != null) {

			return "carta piu alta: " + cartaPiuAlta.toString();

		}
		return null;
		
	}

}
