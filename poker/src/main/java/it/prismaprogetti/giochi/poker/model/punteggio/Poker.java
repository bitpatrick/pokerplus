package it.prismaprogetti.giochi.poker.model.punteggio;

import java.util.ArrayList;
import java.util.List;

import it.prismaprogetti.giochi.poker.model.Carta;

public class Poker implements Punto {

	private final List<Carta[]> pokers;

	public Poker(List<Carta[]> pokers) {
		super();
		this.pokers = pokers;
	}

	public List<Carta[]> getPokers() {
		return pokers;
	}

	public static List<Carta[]> valuta(Carta[] carte) {

		List<Carta[]> pokers = null;

		int qtaCarte = carte.length;

		for (int i = 0; i < qtaCarte; i++) {

			for (int j = i + 1; j < qtaCarte; j++) {

				if (carte[i].getValore().getNumero() == carte[j].getValore().getNumero()) {

					for (int k = j + 1; k < qtaCarte; k++) {

						if (carte[i].getValore().getNumero() == carte[k].getValore().getNumero()) {

							for (int u = k + 1; u < qtaCarte; u++) {

								if (carte[i].getValore().getNumero() == carte[u].getValore().getNumero()) {
									
									if (pokers == null) {

										pokers = new ArrayList<>(1);
									}

									Carta[] poker = { carte[i], carte[j], carte[k], carte[u] };
									pokers.add(poker);
								}
								

							}

						}

					}

				}

			}

		}

		return pokers;

	}

	@Override
	public String info() {

		List<Carta[]> pokers = this.pokers;
		if (pokers != null) {

			String s = "";

			for (Carta[] poker : pokers) {

				s += "poker: " + poker[0] + " " + poker[1] + " " + poker[2] + " " + poker[3];

			}

			return s;
		}
		return null;
		
	}

}
