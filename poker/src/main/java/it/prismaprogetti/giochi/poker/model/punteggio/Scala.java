package it.prismaprogetti.giochi.poker.model.punteggio;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Queue;

import it.prismaprogetti.giochi.poker.model.Carta;

public class Scala implements Punto {

	public static final int qtaCarteScala = 5;

	private final List<List<Carta[]>> scale;

	public Scala(List<List<Carta[]>> scale) {
		super();
		this.scale = scale;
	}

	public static int getQtacartescala() {
		return qtaCarteScala;
	}

	public List<List<Carta[]>> getScale() {
		return scale;
	}

	public static List<List<Carta[]>> valuta(Carta[] carte) {

		List<Carta> carteNew = new ArrayList<>(5); // 5 � la grandezza predefinita

		for (Carta carta : carte) {

			carteNew.add(carta);
		}

		Collections.sort(carteNew);

		List<Carta[]> scale = calcolaScale(carteNew);
		List<Carta[]> scaleInverse = calcolaScale(reverse(carteNew));

		List<List<Carta[]>> combinazioniDiScale = new ArrayList<>(2);
		combinazioniDiScale.add(scale);
		combinazioniDiScale.add(scaleInverse);

		return combinazioniDiScale;

	}

	private static List<Carta> reverse(List<Carta> carte) {

		List<Carta> carteNew = null;
		Queue<Carta> assi = null;
		int qtaAssi = 0;
		for (Carta carta : carte) {

			if (carta.getValore().getSimbolo().equals("A")) {

				if (assi == null) {
					assi = new ArrayDeque<>();
				}

				carta.getValore().setNumero(1);
				assi.add(carta);
				qtaAssi++;
			}
		}
		if (qtaAssi > 0) {

			carteNew = new ArrayList<>(carte.subList(qtaAssi, carte.size()));
			for (int a = 0; a < qtaAssi; a++) {
				carteNew.add(assi.poll());
			}
			Collections.reverse(carteNew);
		}
		return carteNew;

	}

	private static List<Carta[]> calcolaScale(List<Carta> carteInManoOrdinate) {

		if (carteInManoOrdinate == null) {
			return null;
		}
		int qtaScale = howManyScale(carteInManoOrdinate);
		List<Carta[]> scale = null;

		int indiceScala = 0;
		for (int i = 0; i < qtaScale; i++) {

			if (

			(carteInManoOrdinate.get(indiceScala).getValore().getNumero() - 1 == carteInManoOrdinate
					.get(indiceScala + 1).getValore().getNumero()
					&& carteInManoOrdinate.get(indiceScala + 1).getValore().getNumero() - 1 == carteInManoOrdinate
							.get(indiceScala + 2).getValore().getNumero()
					&& carteInManoOrdinate.get(indiceScala + 2).getValore().getNumero() - 1 == carteInManoOrdinate
							.get(indiceScala + 3).getValore().getNumero()
					&& carteInManoOrdinate.get(indiceScala + 3).getValore().getNumero() - 1 == carteInManoOrdinate
							.get(indiceScala + 4).getValore().getNumero())

					||

					(carteInManoOrdinate.get(indiceScala).getValore().getNumero() + 1 == carteInManoOrdinate
							.get(indiceScala + 1).getValore().getNumero()
							&& carteInManoOrdinate.get(indiceScala + 1).getValore().getNumero()
									+ 1 == carteInManoOrdinate.get(indiceScala + 2).getValore().getNumero()
							&& carteInManoOrdinate.get(indiceScala + 2).getValore().getNumero()
									+ 1 == carteInManoOrdinate.get(indiceScala + 3).getValore().getNumero()
							&& carteInManoOrdinate.get(indiceScala + 3).getValore().getNumero()
									+ 1 == carteInManoOrdinate.get(indiceScala + 4).getValore().getNumero())

			) {

				Carta[] scala = new Carta[5];

				scala[0] = carteInManoOrdinate.get(indiceScala + 0);
				scala[1] = carteInManoOrdinate.get(indiceScala + 1);
				scala[2] = carteInManoOrdinate.get(indiceScala + 2);
				scala[3] = carteInManoOrdinate.get(indiceScala + 3);
				scala[4] = carteInManoOrdinate.get(indiceScala + 4);

				if (scale == null) {
					scale = new ArrayList<>(1);
				}

				scale.add(scala);
				indiceScala += qtaCarteScala;

			}

		}
		return scale;
	}

	private static int howManyScale(List<Carta> carte) {

		if (carte.size() < qtaCarteScala) {
			throw new NumberFormatException("il numero di carte per almeno una scala non pu� essere inferiore a 5");
		}

		return Math.floorDiv(carte.size(), qtaCarteScala);

	}

	@Override
	public String info() {

		List<List<Carta[]>> scale = this.scale;
		if (scale != null) {

			String s = "";

			for (Carta[] scala : scale.get(0)) {

				s += "scalReale: " + scala[0] + " " + scala[1] + " " + scala[2] + " " + scala[3] + " " + scala[4];

			}

			for (Carta[] scalaInversa : scale.get(1)) {

				s += "scalReale: " + scalaInversa[0] + " " + scalaInversa[1] + " " + scalaInversa[2] + " "
						+ scalaInversa[3] + " " + scalaInversa[4];

			}

			return s;
		}

		return null;

	}

}
