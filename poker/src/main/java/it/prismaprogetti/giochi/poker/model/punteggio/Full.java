package it.prismaprogetti.giochi.poker.model.punteggio;

import java.util.ArrayList;
import java.util.List;

import it.prismaprogetti.giochi.poker.model.Carta;

public class Full implements Punto {

	public static final int qtaCarteFull = 5;

	private final List<Carta[]> fulls;

	public Full(List<Carta[]> fulls) {
		super();
		this.fulls = fulls;
	}

	public List<Carta[]> getFulls() {
		return fulls;
	}

	public static List<Carta[]> valuta(Carta[] carte) {

		/*
		 * verifico la qt� dei fulls generabili
		 */
		int qtaFulls = howManyFulls(carte);

		/*
		 * recupero i tris dalle carte
		 */
		List<Carta[]> triss = Tris.valuta(carte);
		
		if( triss == null ) {
			
			return null;
		}
		int qtaCarteDeiTris = triss.size() * 3;

		/*
		 * array che passer� per calcolare le coppie questo array non dovr� contenere le
		 * carte presenti nei triss
		 */
		Carta[] carteNew = new Carta[carte.length - qtaCarteDeiTris];

		/*
		 * recupero solo i valori numerici dei triss es: [7,7,7] --> 7
		 */
		List<Integer> valoriNumericiDeiTris = new ArrayList<>();
		int indiceTris = 0;
		for (Carta[] tris : triss) {

			valoriNumericiDeiTris.add(tris[indiceTris++].getValore().getNumero());
		}

		int indiceCarteNew = 0;
		for (int i = 0; i < carte.length; i++) {

			if (!valoriNumericiDeiTris.contains(carte[i].getValore().getNumero())) {

				carteNew[indiceCarteNew++] = carte[i];
			}

		}

		List<Carta[]> fulls = new ArrayList<>();
		List<Carta[]> coppie = Coppia.valuta(carteNew);
		
		if ( coppie == null ) {
			
			return null;
		}

		for (int j = 0; j < qtaFulls; j++) {

			Carta[] full = new Carta[5];
			Carta[] tris = triss.get(j);
			Carta[] coppia = coppie.get(j);

			/*
			 * devo unire tris e coppia : devono formare 5 carte un full
			 */
			full[0] = tris[0];
			full[1] = tris[1];
			full[2] = tris[2];
			full[3] = coppia[0];
			full[4] = coppia[1];

			fulls.add(full);
		}

		return fulls;

	}

	private static int howManyFulls(Carta[] carte) {

		if (carte.length < qtaCarteFull) {
			throw new NumberFormatException("il numero di carte per almeno un solo full non pu� essere inferiore a 5");
		}

		return Math.floorDiv(carte.length, qtaCarteFull);

	}

	@Override
	public String info() {
		
		List<Carta[]> fulls = this.fulls;
		if (fulls != null) {

			String s = "";

			for (Carta[] full : fulls) {

				s += "scalReale: " + full[0] + " " + full[1] + " " + full[2] + " " + full[3] + " "
						+ full[4];

			}

			return s;
		}
		return null;
		
	}

}
