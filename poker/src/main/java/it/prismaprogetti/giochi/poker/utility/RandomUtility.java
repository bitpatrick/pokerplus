package it.prismaprogetti.giochi.poker.utility;

public class RandomUtility {
	
	public static int randomNumber(int min, int max) {

		return (int) Math.floor(Math.random() * ((max - min + 1) + min));
	}

}
