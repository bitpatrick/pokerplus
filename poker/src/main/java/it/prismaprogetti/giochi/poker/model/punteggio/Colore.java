package it.prismaprogetti.giochi.poker.model.punteggio;

import java.util.ArrayList;
import java.util.List;

import it.prismaprogetti.giochi.poker.model.Carta;

public class Colore implements Punto {

	private final List<Carta[]> colori;

	public static final int qtaCarteColore = 5;

	public Colore(List<Carta[]> colori) {
		super();
		this.colori = colori;
	}

	public List<Carta[]> getColori() {
		return colori;
	}

	public static int getQtacartecolore() {
		return qtaCarteColore;
	}

	public static List<Carta[]> valuta(Carta[] carte) {

		List<Carta[]> colori = null;

		int qtaColori = howManyColours(carte);

		int indiceCarte = 0;
		for (int i = 0; i < qtaColori; i++) {

			Carta[] colore = new Carta[qtaCarteColore];

			if (carte[indiceCarte].getSeme() != carte[indiceCarte + 1].getSeme()
					|| carte[indiceCarte].getSeme() != carte[indiceCarte + 2].getSeme()
					|| carte[indiceCarte].getSeme() != carte[indiceCarte + 3].getSeme()
					|| carte[indiceCarte].getSeme() != carte[indiceCarte + 4].getSeme()) {
				continue;
			}

			colore[0] = carte[indiceCarte];
			colore[1] = carte[indiceCarte + 1];
			colore[2] = carte[indiceCarte + 2];
			colore[3] = carte[indiceCarte + 3];
			colore[4] = carte[indiceCarte + 4];

			if (colori == null) {

				colori = new ArrayList<>(qtaColori);
			}

			colori.add(colore);
			indiceCarte += 5;

		}

		return colori;

	}

	private static int howManyColours(Carta[] carte) {

		return Math.floorDiv(carte.length, qtaCarteColore);

	}

	@Override
	public String info() {
		
		List<Carta[]> colori = this.colori;
		if (colori != null) {

			String s = "";

			for (Carta[] colore : colori) {

				s += "scalReale: " + colore[0] + " " + colore[1] + " " + colore[2] + " " + colore[3] + " "
						+ colore[4];

			}

			return s;
		}
		return null;

		
	}

}
