package it.prismaprogetti.giochi.poker.model;

public class Valore {

	private String simbolo;
	private String nome;
	private int numero;

	private Valore(String simbolo, String nome, int numero) {
		super();
		this.simbolo = simbolo;
		this.nome = nome;
		this.numero = numero;
	}

	public String getSimbolo() {
		return simbolo;
	}

	public String getNome() {
		return nome;
	}

	public int getNumero() {
		return numero;
	}
	
	

	public void setNumero(int numero) {
		this.numero = numero;
	}

	private static void checkParametriValoreAreNotNull(String simbolo, String valoreStringa, int valoreNumerico,
			int valoreMinimo, int valoreMassimo) {

		if ((simbolo == null || valoreStringa == null || valoreNumerico == 0)
				|| (simbolo.isBlank() || valoreStringa.isBlank())) {
			throw new IllegalAccessError("il valore della carta sia stringa che numerico non possono essere nulli");
		}

		if (valoreNumerico < valoreMinimo || valoreNumerico > valoreMassimo) {
			throw new NumberFormatException("il valore numerico non pu� essere minore di zero oppure maggiore di 13");
		}
	}

	public static Valore crea(String simbolo, String valoreStringa, int valoreNumerico, int valoreMinimo,
			int valoreMassimo) {

		checkParametriValoreAreNotNull(simbolo, valoreStringa, valoreNumerico, valoreMinimo, valoreMassimo);

		return new Valore(simbolo, valoreStringa, valoreNumerico);
	}

	public static Valore creaValoriCartePoker(int valoreNumerico) {

		String simbolo = "";
		String valoreStringa = "";

		switch (valoreNumerico) {

		case 2:
			valoreStringa = "DUE";
			simbolo = "2";
			break;
		case 3:
			valoreStringa = "TRE";
			simbolo = "3";
			break;
		case 4:
			valoreStringa = "QUATTRO";
			simbolo = "4";
			break;
		case 5:
			valoreStringa = "CINQUE";
			simbolo = "5";
			break;
		case 6:
			valoreStringa = "SEI";
			simbolo = "6";
			break;
		case 7:
			valoreStringa = "SETTE";
			simbolo = "7";
			break;
		case 8:
			valoreStringa = "OTTO";
			simbolo = "8";
			break;
		case 9:
			valoreStringa = "NOVE";
			simbolo = "9";
			break;
		case 10:
			valoreStringa = "DIECI";
			simbolo = "10";
			break;
		case 11:
			valoreStringa = "JACK";
			simbolo = "J";
			break;
		case 12:
			valoreStringa = "DONNA";
			simbolo = "Q";
			break;
		case 13:
			valoreStringa = "RE";
			simbolo = "K";
			break;
		case 14:
			valoreStringa = "ASSO";
			simbolo = "A";
			break;
		}

		return crea(simbolo, valoreStringa, valoreNumerico, 2, 14);
	}

}
