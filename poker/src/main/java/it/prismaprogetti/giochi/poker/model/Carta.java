package it.prismaprogetti.giochi.poker.model;

public class Carta implements Comparable<Carta> {

	private Seme seme;
	private Valore valore;

	private Carta(Seme seme, Valore valore) {
		super();
		this.seme = seme;
		this.valore = valore;
	}

	public Seme getSeme() {
		return seme;
	}

	public Valore getValore() {
		return valore;
	}

	public static void checkCartaIsNotNull(Seme seme, Valore valore) {

		if (seme == null || valore == null) {
			throw new IllegalStateException("il seme e/o il valore carta non possono essere nulli");
		}
	}

	public static Carta crea(Seme seme, Valore valore) {

		checkCartaIsNotNull(seme, valore);

		return new Carta(seme, valore);
	}

	@Override
	public int hashCode() {
		return this.valore.getNumero() + this.seme.hashCode();
	}

	@Override
	public boolean equals(Object obj) {

		if (obj == null) {
			return false;
		}
		if (obj instanceof Carta) {
			Carta carta = (Carta) obj;
			return carta.getValore().getNumero() == this.getValore().getNumero()
					&& carta.getSeme().equals(this.getSeme());
		}
		return false;

	}

	@Override
	public int compareTo(Carta carta) {
		
		if ( this.getValore().getSimbolo().equals("A") && this.getValore().getNumero() == 1 ) {
			
			this.getValore().setNumero(14);
		}
		if ( carta.getValore().getSimbolo().equals("A") && carta.getValore().getNumero() == 1 ) {
			
			carta.getValore().setNumero(14);
		}

		if (this.getValore().getNumero() > carta.getValore().getNumero()) {

			return -1;

		} else if (this.getValore().getNumero() < carta.getValore().getNumero()) {

			return 1;

		} else if (this.getValore().getNumero() == carta.getValore().getNumero()) {

			if (this.getSeme().getValoreSeme() > carta.getSeme().getValoreSeme()) {

				return -1;

			} else if (this.getSeme().getValoreSeme() < carta.getSeme().getValoreSeme()) {

				return 1;
			}

		}

		return 0;
	}

	@Override
	public String toString() {

		String carta = this.getValore().getSimbolo() + this.getSeme().getSimbolo();

		return carta;
	}

}
