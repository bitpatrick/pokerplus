package it.prismaprogetti.giochi.poker.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import it.prismaprogetti.giochi.poker.model.punteggio.CartaPiuAlta;
import it.prismaprogetti.giochi.poker.model.punteggio.Colore;
import it.prismaprogetti.giochi.poker.model.punteggio.Coppia;
import it.prismaprogetti.giochi.poker.model.punteggio.DoppiaCoppia;
import it.prismaprogetti.giochi.poker.model.punteggio.Full;
import it.prismaprogetti.giochi.poker.model.punteggio.Poker;
import it.prismaprogetti.giochi.poker.model.punteggio.Punto;
import it.prismaprogetti.giochi.poker.model.punteggio.Scala;
import it.prismaprogetti.giochi.poker.model.punteggio.ScalaReale;
import it.prismaprogetti.giochi.poker.model.punteggio.Tris;

public class Mano {

	private String nomePartita;
	private Punto puntoDellaMano;
	private Carta[] carte;

	public Mano(String nomePartita, Carta[] carte) {
		super();
		this.nomePartita = nomePartita;
		this.carte = carte;
	}

	public String getNomePartita() {
		return nomePartita;
	}

	public Punto getPuntoDellaMano() {
		return puntoDellaMano;
	}

	public Carta[] getCarte() {
		return carte;
	}

	public String valutaPunto() {

		List<Carta[]> scaleReali = ScalaReale.valuta(carte);
		if (scaleReali != null && this.puntoDellaMano == null) {

			ScalaReale scalaReale = new ScalaReale(scaleReali);
			this.puntoDellaMano = scalaReale;
			return this.puntoDellaMano.info();
		}

		List<Carta[]> pokers = Poker.valuta(carte);
		if (pokers != null && this.puntoDellaMano == null) {

			Poker poker = new Poker(pokers);
			this.puntoDellaMano = poker;
			return this.puntoDellaMano.info();
		}

		List<Carta[]> colori = Colore.valuta(carte);
		if (colori != null && this.puntoDellaMano == null) {

			Colore colore = new Colore(colori);
			this.puntoDellaMano = colore;
			return this.puntoDellaMano.info();
		}

		List<Carta[]> fulls = Full.valuta(carte);
		if (fulls != null && this.puntoDellaMano == null) {

			Full full = new Full(fulls);
			this.puntoDellaMano = full;
			return this.puntoDellaMano.info();
		}

		List<List<Carta[]>> scale = Scala.valuta(carte);
		if ((scale.get(0) != null || scale.get(1) != null) && this.puntoDellaMano == null) {

			Scala scala = new Scala(scale);
			this.puntoDellaMano = scala;
			return this.puntoDellaMano.info();
		}

		List<Carta[]> triss = Tris.valuta(carte);
		if (triss != null && this.puntoDellaMano == null) {

			Tris tris = new Tris(triss);
			this.puntoDellaMano = tris;
			return this.puntoDellaMano.info();
		}

		List<Carta[]> doppieCoppie = DoppiaCoppia.valuta(carte);
		if (doppieCoppie != null && this.puntoDellaMano == null) {

			DoppiaCoppia doppiaCoppia = new DoppiaCoppia(doppieCoppie);
			this.puntoDellaMano = doppiaCoppia;
			return this.puntoDellaMano.info();
		}

		List<Carta[]> coppie = Coppia.valuta(carte);
		if (coppie != null && this.puntoDellaMano == null) {

			Coppia coppia = new Coppia(coppie);
			this.puntoDellaMano = coppia;
			return this.puntoDellaMano.info();
		}

		Carta cartaSingola = CartaPiuAlta.valuta(carte);
		if (cartaSingola != null && this.puntoDellaMano == null) {

			CartaPiuAlta cartaPiuAlta = new CartaPiuAlta(cartaSingola);
			this.puntoDellaMano = cartaPiuAlta;
			return this.puntoDellaMano.info();
		}

		return null;

	}

	public static Carta[] ordinaCarte(Carta[] carte) {

		List<Carta> carteInMano = new ArrayList<>(Arrays.asList(carte));

		Collections.sort(carteInMano);

		return carteInMano.toArray(new Carta[5]);

	}

}
