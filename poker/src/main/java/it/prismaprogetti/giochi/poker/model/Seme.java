package it.prismaprogetti.giochi.poker.model;

public enum Seme {

	CUORI('#', "Come", 4), QUADRI('?', "Quando", 3), FIORI('*', "Fuori", 2), PICCHE('^', "Piove", 1);

	private final char simbolo;
	private final String proverbio;
	private final int valoreSeme;

	private Seme(char simbolo, String proverbio, int valoreSeme) {
		this.simbolo = simbolo;
		this.proverbio = proverbio;
		this.valoreSeme = valoreSeme;
	}

	public char getSimbolo() {
		return simbolo;
	}

	public String getProverbio() {
		return proverbio;
	}

	public int getValoreSeme() {
		return valoreSeme;
	}

}
