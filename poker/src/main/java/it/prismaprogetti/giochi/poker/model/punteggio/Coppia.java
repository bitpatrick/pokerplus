package it.prismaprogetti.giochi.poker.model.punteggio;

import java.util.ArrayList;
import java.util.List;

import it.prismaprogetti.giochi.poker.model.Carta;

public class Coppia implements Punto {

	private final List<Carta[]> coppie;

	public Coppia(List<Carta[]> coppie) {
		super();
		this.coppie = coppie;
	}

	public List<Carta[]> getCoppie() {
		return coppie;
	}

	public static List<Carta[]> valuta(Carta[] carte) {

		List<Carta[]> listaDiCoppie = null;
		int qtaCarte = carte.length;

		for (int i = 0; i < qtaCarte; i++) {

			for (int j = i + 1; j < qtaCarte; j++) {

				if (carte[i].getValore().getNumero() == carte[j].getValore().getNumero()) {

					if (listaDiCoppie == null) {
						listaDiCoppie = new ArrayList<>(1);
					}

					Carta[] coppia = { carte[i], carte[j] };
					listaDiCoppie.add(coppia);
				}

			}

		}

		return listaDiCoppie;
	}

	@Override
	public String info() {
		
		List<Carta[]> coppie = this.coppie;
		if (coppie != null) {

			String s = "";

			for (Carta[] coppia : coppie) {

				s += "coppia: " + coppia[0] + " " + coppia[1];

			}

			return s;
		}
		return null;
		
	}

}
