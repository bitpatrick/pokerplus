package it.prismaprogetti.giochi.poker.model.punteggio;

import java.util.List;

import it.prismaprogetti.giochi.poker.model.Carta;

public class ScalaReale implements Punto {

	private final List<Carta[]> scaleReali;

	public ScalaReale(List<Carta[]> scaleReali) {
		super();
		this.scaleReali = scaleReali;
	}

	public List<Carta[]> getScaleReali() {
		return scaleReali;
	}

	public static List<Carta[]> valuta(Carta[] carte) {

		List<Carta[]> carteColore = Colore.valuta(carte);
		List<List<Carta[]>> carteScala = Scala.valuta(carte);

		if (carteColore == null || carteScala == null) {

			return null;
		}

		if (carteScala.get(0) == null && carteScala.get(1) != null) {

			return carteScala.get(1);
		}

		return carteScala.get(0);

	}

	@Override
	public String info() {

		List<Carta[]> scaleReali = this.scaleReali;
		if (scaleReali != null) {

			String s = "";

			for (Carta[] scalReale : scaleReali) {

				s += "scalReale: " + scalReale[0] + " " + scalReale[1] + " " + scalReale[2] + " " + scalReale[3] + " "
						+ scalReale[4];

			}

			return s;
		}
		
		return null;

	}

}
