package it.prismaprogetti.giochi.poker.model.punteggio;

import java.util.ArrayList;
import java.util.List;

import it.prismaprogetti.giochi.poker.model.Carta;

public class DoppiaCoppia implements Punto {

	private final List<Carta[]> doppieCoppie;

	public DoppiaCoppia(List<Carta[]> doppieCoppie) {
		super();
		this.doppieCoppie = doppieCoppie;
	}

	public List<Carta[]> getDoppieCoppie() {
		return doppieCoppie;
	}

	public static List<Carta[]> valuta(Carta[] carte) {

		List<Carta[]> coppie = Coppia.valuta(carte);// mi restituisce tutte le coppie

		if (coppie == null || coppie.size() == 1 || Tris.valuta(carte) != null) {

			return null;
		}

		List<Carta[]> doppieCoppie = new ArrayList<>();

		if (coppie.size() % 2 != 0) {

			Carta[] coppiaScartata = coppie.remove(coppie.size() - 1);

			System.err.println("La lista delle coppie � dispari, viene eliminata l'ultima coppia della lista:  "
					+ coppiaScartata.toString());
		}

		// { [x,x] , [y,y] , [k,k] , [z,z] }

		// lunghezzaListaCoppie * numeroCheFormaUNAcoppia = numeroDiCoppie
		int qtaDiCarte = coppie.size() * 2;
		int qtaDoppieCoppie = qtaDiCarte / 4;

		for (int i = 0; i < qtaDoppieCoppie; i++) {

			Carta[] doppiaCoppia = new Carta[4];

			Carta[] primaCoppia = coppie.get(i);
			Carta[] secondaCoppia = coppie.get(i + 1);

			doppiaCoppia[0] = primaCoppia[0];
			doppiaCoppia[1] = primaCoppia[1];
			doppiaCoppia[2] = secondaCoppia[0];
			doppiaCoppia[3] = secondaCoppia[1];

			doppieCoppie.add(doppiaCoppia);
		}

		return doppieCoppie;
	}

	@Override
	public String info() {

		List<Carta[]> doppieCoppie = this.doppieCoppie;
		if (doppieCoppie != null) {

			String s = "";

			for (Carta[] doppiaCoppia : doppieCoppie) {

				s += "doppia coppia: " + doppiaCoppia[0] + " " + doppiaCoppia[1] + " " + doppiaCoppia[2] + " "
						+ doppiaCoppia[3];

			}

			return s;
		}
		return null;
		
	}
}