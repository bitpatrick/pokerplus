package it.prismaprogetti.giochi.poker.model.punteggio;

import java.util.ArrayList;
import java.util.List;

import it.prismaprogetti.giochi.poker.model.Carta;

public class Tris implements Punto {

	private final List<Carta[]> triss;

	public Tris(List<Carta[]> triss) {
		super();
		this.triss = triss;
	}

	public List<Carta[]> getTriss() {
		return triss;
	}

	public static List<Carta[]> valuta(Carta[] carte) {

		List<Carta[]> triss = null;

		int qtaCarte = carte.length;

		for (int i = 0; i < qtaCarte; i++) {

			for (int j = i + 1; j < qtaCarte; j++) {

				if (carte[i].getValore().getNumero() == carte[j].getValore().getNumero()) {

					for (int k = j + 1; k < qtaCarte; k++) {

						if (carte[i].getValore().getNumero() == carte[k].getValore().getNumero()) {

							if (triss == null) {

								triss = new ArrayList<>(1);
							}

							Carta[] tris = { carte[i], carte[j], carte[k] };
							triss.add(tris);
						}

					}

				}

			}

		}

		return triss;

	}

	@Override
	public String info() {

		List<Carta[]> triss = this.triss;
		if (triss != null) {

			String s = "";

			for (Carta[] tris : triss) {

				s += "tris: " + tris[0] + " " + tris[1] + " " + tris[2];

			}

			return s;
		}
		return null;

	}

}
