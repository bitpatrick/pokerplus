package it.prismaprogetti.giochi.poker.run;

import it.prismaprogetti.giochi.poker.model.Carta;
import it.prismaprogetti.giochi.poker.model.Mano;
import it.prismaprogetti.giochi.poker.model.Mazzo;

import static it.prismaprogetti.giochi.poker.model.Seme.*;

public class Main {

	public static void main(String[] args) {
		
		
		
		Mazzo mazzo = Mazzo.nuovoMazzoCartePoker("Mondiano");
//		
//		Carta carta1 = mazzo.prendiCartaOrNull("J", CUORI);
//		Carta carta2 = mazzo.prendiCartaOrNull("J", FIORI);
//		Carta carta3 = mazzo.prendiCartaOrNull("J", PICCHE);
//		Carta carta4 = mazzo.prendiCartaOrNull("5", CUORI);
//		Carta carta5 = mazzo.prendiCartaOrNull("3", CUORI);
//		
//		Carta[] carte = { carta1, carta2, carta3, carta4, carta5 };
//		
//		Mano mano = new Mano("partita prova");
//		
//		mano.valutaPunto(carte);
		
		
		mazzo.mischiaCarte();
//		
		Carta[] carteInMano = mazzo.daiCinqueCarte();
		System.out.println("Carte in mano:");
		for (Carta carta : carteInMano) {
			System.out.print(carta + " ");
		}
		System.out.println("\n////////////////////////////////");
		
		Carta[] carteInManoOrdinate = Mano.ordinaCarte(carteInMano);
		System.out.println("Carte in mano ordinate:");
		for (Carta carta : carteInManoOrdinate) {
			System.out.print(carta + " ");
		}
		System.out.println("\n////////////////////////////////");
//		
//		
		Mano mano = new Mano("partita", carteInManoOrdinate);
		
//		
		String puntoPartita = mano.valutaPunto();
		
		System.out.println(puntoPartita);
//		
//		
////		Carta carta6 = mazzo.prendiCartaOrNull("K", CUORI);
////		Carta carta7 = mazzo.prendiCartaOrNull("Q", CUORI);
//		Carta carta8 = mazzo.prendiCartaOrNull("10", CUORI);
//		Carta carta9 = mazzo.prendiCartaOrNull("J", CUORI);
//		Carta carta10 = mazzo.prendiCartaOrNull("6", CUORI);
////		Carta carta11 = mazzo.prendiCartaOrNull(7, CUORI);
		
//		Carta[] carte = { carta1, carta2, carta3, carta4, carta5, carta6, carta7, carta8, carta9, carta10 };
		
	
//		Mano mano = new Mano();

//		ScalaReale.valuta(carte);
//		Colore.verifica(carte);
//		Scala.verifica(carte);
//		Full.valuta(carte);
//		List<Carta[]> triss = Tris.valuta(carte);
//		List<Carta[]> doppieCoppie = DoppiaCoppia.valuta(carte);
//		List<Carta[]> coppie = Coppia.valuta(carte);
//		Carta cartaPiuAlta = CartaSingola.valuta(carte);		
		
		

		
		
	}

}